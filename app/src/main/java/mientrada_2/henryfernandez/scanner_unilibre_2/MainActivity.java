package mientrada_2.henryfernandez.scanner_unilibre_2;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton mFloatingActionButton;
    TextView txtViewUnicode, txtIcono;
    ProgressBar progressBar;
    int hasCameraPermission;

    private void AccessPermition(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA);
            if(hasCameraPermission != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.CAMERA},1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode){
            case 1:
                if(grantResults[0]  == PackageManager.PERMISSION_GRANTED){
                    mFloatingActionButton.performClick();
                }else{
                    Toast.makeText(this, "Permiso negado", Toast.LENGTH_SHORT).show();
                    txtViewUnicode.setText("Permiso negado");
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar)findViewById(R.id.cargando);
        txtViewUnicode = (TextView) findViewById(R.id.txtUnicode);
        txtIcono = (TextView) findViewById(R.id.txtIcono);
        txtIcono.setTypeface(FuenteAwesome(this));
        txtIcono.setTextSize(70);
        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.Scann);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AccessPermition();
                try {
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                    integrator.setPrompt("Universidad libre, Henry Fernandez.");
                    integrator.setCameraId(0);
                    integrator.setBeepEnabled(true);
                    integrator.initiateScan();
                } catch (Exception e) {
                    e.printStackTrace();
                    txtViewUnicode.setText(e.toString());
                }
            }
        });
    }

    public void buscarRegistro(final String identificacion, final String nombres) {
        progressBar.setVisibility(View.VISIBLE);
        txtIcono.setVisibility(View.GONE);
        txtViewUnicode.setVisibility(View.GONE);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                "--url removida por seguridad--" + identificacion,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("success")) {
                                JSONObject json = response.getJSONArray("informacion").getJSONObject(0);
                                String nombre = json.getString("codgradoscursos");
                                String identificacion = json.getString("descripcion");
                                txtIcono.setText("\uf165");
                                txtIcono.setTextColor(getResources().getColor(R.color.colorAccent));
                                txtViewUnicode.setText(nombre.toUpperCase() +" "+ " con la identificacion " + identificacion + " ya ingresó : " +json.getString("orden"));
                                txtViewUnicode.setTextColor(MainActivity.this.getResources().getColor(R.color.colorAccent));
                                progressBar.setVisibility(View.GONE);
                                txtIcono.setVisibility(View.VISIBLE);
                                txtViewUnicode.setVisibility(View.VISIBLE);
                            }else{
                                RegistrarIngreso(identificacion,nombres);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("No se pudo validar el registro");
                alertDialog.setMessage("Revisa la conexión a internet.");
                alertDialog.setButton(-1, (CharSequence) "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
                Toast.makeText(MainActivity.this, "No conecto al servidor", Toast.LENGTH_SHORT).show();
            }
        });
        volleySingle.getInstance(MainActivity.this).getfRequestQueue().add(jsonObjectRequest);
    }

    public void RegistrarIngreso(String id, final String nombres) {
        final String str = id;
        final String str2 = nombres;
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                "-url-eliminada-seguridad-",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            if (new JSONObject(response).getBoolean("success")) {
                                txtViewUnicode.setTextColor(getResources().getColor(R.color.colorSuccess));
                                txtIcono.setText("\uf164");
                                txtIcono.setTextColor(getColor(R.color.colorSuccess));
                                txtViewUnicode.setText("Registro exitoso!, " + nombres.toUpperCase() + " puede ingrear");
                                progressBar.setVisibility(View.GONE);
                                txtIcono.setVisibility(View.VISIBLE);
                                txtViewUnicode.setVisibility(View.VISIBLE);
                                return;
                            } else {
                                //MainActivity.this.cargando.setVisibility(8);
                                progressBar.setVisibility(View.GONE);
                                txtIcono.setVisibility(View.VISIBLE);
                                txtViewUnicode.setVisibility(View.VISIBLE);
                                txtIcono.setText("\uf165");
                                txtIcono.setTextColor(getColor(R.color.colorAccent));
                                txtViewUnicode.setTextColor(MainActivity.this.getResources().getColor(R.color.colorAccent));
                                txtViewUnicode.setText("Lo sentimos no pudo ser registrado el ingreso.");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("No se pudo hacer el registro");
                alertDialog.setMessage("Revisa la conexión a internet.");
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setCancelable(false);
                alertDialog.setButton(-1, (CharSequence) "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alertDialog.show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> localHashMap = new HashMap<>();
                localHashMap.put("i", str);
                localHashMap.put("n", str2);
                return localHashMap;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(8000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        volleySingle.getInstance(this).getfRequestQueue().add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Escanner cancelado", Toast.LENGTH_SHORT).show();
                txtViewUnicode.setText("Escanner cancelado");
            } else {
                try {
                    JSONObject objeto = new JSONObject(result.getContents().toString());
                    Long desc = Long.parseLong(objeto.getString("identificacion"));
                    if (desc > 0) {
                        buscarRegistro(objeto.getString("identificacion"), objeto.getString("nombres"));
                    }else{
                        txtViewUnicode.setText("Identificacion no valida");
                    }
                } catch (Exception e) {
                    Toast.makeText(this, "No se pudo scannear", Toast.LENGTH_LONG).show();
                    txtViewUnicode.setText(e.toString());
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static Typeface FuenteAwesome(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome-webfont.ttf");
    }
}
